// GERADOR DE OBJETOS //////////////////////////////////////////////////////////////////////////////

const randomNumber = limit => Math.floor(Math.random() * limit);
let randomColor = ["red", "blue", "yellow", "green", "orchid"]

//Cria Pixeis para Objeto
const criaPixel = (nome, color) => {
    let pixel = document.createElement('DIV');
    pixel.id = nome
    pixel.classList.add('pixel')
    document.getElementById("conteudo").appendChild(pixel);
    pixel.style.top = '0px';
    pixel.style.left = '100px';
    pixel.style.background = `radial-gradient(white, ${color} 90%)`;
    pixel.style.borderColor = `dark${color}`
    if (color === "yellow") {
        pixel.style.borderColor = `darkorange`
    }
}

// Move Pixel 20px para baixo
const movePixelBaixo = (pixel) =>{
    let cell = document.getElementById(pixel)
    cell.style.top = parseInt(cell.style.top) + 20;
}

//Move Pixel 20px para Frente
const movePixelFrente = (pixel) =>{
    let cell = document.getElementById(pixel)
    cell.style.left = parseInt(cell.style.left) + 20;
}

//Move Pixel 20px para Atraz
const movePixelAtraz = (pixel) =>{
    let cell = document.getElementById(pixel)
    cell.style.left = parseInt(cell.style.left) - 20;
}

// Move Pixel 20px para Cima
const movePixelCima = (pixel) =>{
    let cell = document.getElementById(pixel)
    cell.style.top = parseInt(cell.style.top) - 20;
}

//Variaveis de Objeto
let quantPixel = 0;  // Total de Pixeis ja inseridos
let temp1 = []; // matrizes para criar Objeto e guardar temporariamente nome do Pixel
let temp2 = [];
let temp3 = [];
let temp4 = [];
let tipoObj = ""; // Guarda tipo do Objeto {I, O, S, Z, T, L }
let movimento = 1; // Guarda posicao de giro atual

// Cria o objeto O
const criaO = () => {
    let color = randomColor[randomNumber(randomColor.length)]
    quantPixel++;
    tipoObj = "O";
    temp1[0] = "p" + quantPixel;
    temp1[1] = criaPixel(temp1[0], color);
    document.getElementById(temp1[0]).style.top = "0px";
    document.getElementById(temp1[0]).style.left = "80px";
    
    quantPixel++;
    temp2[0] = "p" + quantPixel;
    temp2[1] = criaPixel(temp2[0], color);
    document.getElementById(temp2[0]).style.top = "0px";
    document.getElementById(temp2[0]).style.left = "100px";
    
    quantPixel++;
    temp3[0] = "p" + quantPixel;
    temp3[1] = criaPixel(temp3[0], color);
    document.getElementById(temp3[0]).style.top = "20px";
    document.getElementById(temp3[0]).style.left = "80px";
    
    quantPixel++;
    temp4[0] = "p" + quantPixel;
    temp4[1] = criaPixel(temp4[0], color);
    document.getElementById(temp4[0]).style.top = "20px";
    document.getElementById(temp4[0]).style.left = "100px";     
}

// Cria o objeto S
const criaS = () => {
    let color = randomColor[randomNumber(randomColor.length)]
    quantPixel++;
    tipoObj = "S";
    temp1[0] = "p" + quantPixel;
    temp1[1] = criaPixel(temp1[0], color);
    document.getElementById(temp1[0]).style.top = "0px";
    document.getElementById(temp1[0]).style.left = "100px";
    
    quantPixel++;
    temp2[0] = "p" + quantPixel;
    temp2[1] = criaPixel(temp2[0], color);
    document.getElementById(temp2[0]).style.top = "0px";
    document.getElementById(temp2[0]).style.left = "120px";
    
    quantPixel++;
    temp3[0] = "p" + quantPixel;
    temp3[1] = criaPixel(temp3[0], color);
    document.getElementById(temp3[0]).style.top = "20px";
    document.getElementById(temp3[0]).style.left = "100px";
    
    quantPixel++;
    temp4[0] = "p" + quantPixel;
    temp4[1] = criaPixel(temp4[0], color);
    document.getElementById(temp4[0]).style.top = "20px";
    document.getElementById(temp4[0]).style.left = "80px";
}

//cria o Objeto Z
const criaZ = () => {
    let color = randomColor[randomNumber(randomColor.length)]
    quantPixel++;
    tipoObj = "Z";
    temp1[0] = "p" + quantPixel;
    temp1[1] = criaPixel(temp1[0], color);
    document.getElementById(temp1[0]).style.top = "0px";
    document.getElementById(temp1[0]).style.left = "80px";
    
    quantPixel++;
    temp2[0] = "p" + quantPixel;
    temp2[1] = criaPixel(temp2[0], color);
    document.getElementById(temp2[0]).style.top = "0px";
    document.getElementById(temp2[0]).style.left = "100px";
    
    quantPixel++;
    temp3[0] = "p" + quantPixel;
    temp3[1] = criaPixel(temp3[0], color);
    document.getElementById(temp3[0]).style.top = "20px";
    document.getElementById(temp3[0]).style.left = "100px";
    
    quantPixel++;
    temp4[0] = "p" + quantPixel;
    temp4[1] = criaPixel(temp4[0], color);
    document.getElementById(temp4[0]).style.top = "20px";
    document.getElementById(temp4[0]).style.left = "120px"; 
}

//cria o Objeto I
const criaI = () => {
    let color = randomColor[randomNumber(randomColor.length)]
    quantPixel++;
    tipoObj = "I";
    temp1[0] = "p" + quantPixel;
    temp1[1] = criaPixel(temp1[0], color);
    document.getElementById(temp1[0]).style.top = "0px";
    document.getElementById(temp1[0]).style.left = "100px";
    
    quantPixel++;
    temp2[0] = "p" + quantPixel;
    temp2[1] = criaPixel(temp2[0], color);
    document.getElementById(temp2[0]).style.top = "20px";
    document.getElementById(temp2[0]).style.left = "100px";
    
    quantPixel++;
    temp3[0] = "p" + quantPixel;
    temp3[1] = criaPixel(temp3[0], color);
    document.getElementById(temp3[0]).style.top = "40px";
    document.getElementById(temp3[0]).style.left = "100px";
    
    quantPixel++;
    temp4[0] = "p" + quantPixel;
    temp4[1] = criaPixel(temp4[0], color);
    document.getElementById(temp4[0]).style.top = "60px";
    document.getElementById(temp4[0]).style.left = "100px";
}

//Cria o Objeto T
const criaT = () => {
    let color = randomColor[randomNumber(randomColor.length)]
    quantPixel++;
    tipoObj = "T";
    temp1[0] = "p" + quantPixel;
    temp1[1] = criaPixel(temp1[0], color);
    document.getElementById(temp1[0]).style.top = "0px";
    document.getElementById(temp1[0]).style.left = "100px";
    
    quantPixel++;
    temp2[0] = "p" + quantPixel;
    temp2[1] = criaPixel(temp2[0], color);
    document.getElementById(temp2[0]).style.top = "20px";
    document.getElementById(temp2[0]).style.left = "80px";
    
    quantPixel++;
    temp3[0] = "p" + quantPixel;
    temp3[1] = criaPixel(temp3[0], color);
    document.getElementById(temp3[0]).style.top = "20px";
    document.getElementById(temp3[0]).style.left = "100px";
    
    quantPixel++;
    temp4[0] = "p" + quantPixel;
    temp4[1] = criaPixel(temp4[0], color);
    document.getElementById(temp4[0]).style.top = "20px";
    document.getElementById(temp4[0]).style.left = "120px";   
}

//Cria o Objeto L
const criaL = () => {
    let color = randomColor[randomNumber(randomColor.length)]
    quantPixel++;
    tipoObj = "L";
    temp1[0] = "p" + quantPixel;
    temp1[1] = criaPixel(temp1[0], color);
    document.getElementById(temp1[0]).style.top = "0px";
    document.getElementById(temp1[0]).style.left = "100px";
    
    quantPixel++;
    temp2[0] = "p" + quantPixel;
    temp2[1] = criaPixel(temp2[0], color);
    document.getElementById(temp2[0]).style.top = "20px";
    document.getElementById(temp2[0]).style.left = "100px";
    
    quantPixel++;
    temp3[0] = "p" + quantPixel;
    temp3[1] = criaPixel(temp3[0], color);
    document.getElementById(temp3[0]).style.top = "40px";
    document.getElementById(temp3[0]).style.left = "100px";
    
    quantPixel++;
    temp4[0] = "p" + quantPixel;
    temp4[1] = criaPixel(temp4[0], color);
    document.getElementById(temp4[0]).style.top = "40px";
    document.getElementById(temp4[0]).style.left = "120px";
}

//Desce o Objeto Atual 20px
const desceObj = () => {
    movePixelBaixo(temp1[0]);
    movePixelBaixo(temp2[0]);
    movePixelBaixo(temp3[0]);
    movePixelBaixo(temp4[0]);
}

//Move para frente o Objeto Atual 20px
const moveFrenteObj = () => {
    movePixelFrente(temp1[0]);
    movePixelFrente(temp2[0]);
    movePixelFrente(temp3[0]);
    movePixelFrente(temp4[0]);
}

//Move para atraz o Objeto Atua 20px
const moveAtrazObj = () => {
    movePixelAtraz(temp1[0]);
    movePixelAtraz(temp2[0]);
    movePixelAtraz(temp3[0]);
    movePixelAtraz(temp4[0]);
}
//GNA  - GERADOR DE NUMEROS ALEATORIOS ////////////////////////////////////////////////////////////////////

let peças = [criaI, criaL, criaO, criaS, criaT, criaZ]

const geraObjRandom = () => {
    peças[randomNumber(peças.length)]()
}
// SUPER DETECTOR DE COLISAO ////////////////////////////////////////////////////////////////////////////////////

//Verifica Colisao de Giro
const verificaColPixel = (nomePixel, tamanho, tipoMov) => {
    let Px = 0;
    let Py = 0;
    let XAlt = parseInt(document.getElementById(nomePixel).style.left);
    let YAlt = parseInt(document.getElementById(nomePixel).style.top);
    switch (tipoMov) {
        case "frente":
            XAlt += tamanho;
            break;
        case "atraz":
            XAlt -= tamanho;
            break;
        case "baixo":
            YAlt += tamanho;            
            break;
        case "cima":
            YAlt -= tamanho;
    }
    
    if ((XAlt < 0) || (XAlt > 180) || (YAlt < 0) || (YAlt > 380)) {
        return true
    }
    for (let i = 1; i < (quantPixel - 3); i++) {
        let col = document.getElementById(`p${i}`)
        Px = parseInt(col.style.left);
        Py = parseInt(col.style.top);
        
        if((Px == XAlt) && (Py == YAlt)) {
            return true
        } 
    }
}

// Gira o Objeto I Atual
const giraI = () => {
    switch (movimento) {
        case 1:
            if(verificaColPixel(temp2[0],20,"cima"))
                break;
            else if(verificaColPixel(temp2[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp3[0],40,"cima"))
                break;
            else if(verificaColPixel(temp3[0],20,"frente"))
                break;
            else if(verificaColPixel(temp4[0],60,"cima"))
                break;
            else if(verificaColPixel(temp4[0],40,"frente"))
                break;
            else
            {           
            movePixelCima(temp2[0]);  //pixel 02
            movePixelAtraz(temp2[0]);
            movePixelCima(temp3[0]); //pixel 03
            movePixelCima(temp3[0]);
            movePixelFrente(temp3[0]);
            movePixelCima(temp4[0]); //pixel 04
            movePixelCima(temp4[0]);
            movePixelCima(temp4[0]);
            movePixelFrente(temp4[0]);
            movePixelFrente(temp4[0]);
            movimento = 2;
            break;
            }
        case 2:
            if(verificaColPixel(temp2[0],20,"baixo"))
                break;
            else if(verificaColPixel(temp2[0],20,"frente"))
                break;
            else if(verificaColPixel(temp3[0],40,"baixo"))
                break;
            else if(verificaColPixel(temp3[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp4[0],60,"baixo"))
                break;
            else if(verificaColPixel(temp4[0],40,"atraz"))
                break;
            else
            {
            movePixelBaixo(temp2[0]);
            movePixelFrente(temp2[0]);
            movePixelBaixo(temp3[0]);
            movePixelBaixo(temp3[0]);
            movePixelAtraz(temp3[0]);
            movePixelBaixo(temp4[0]);
            movePixelBaixo(temp4[0]);
            movePixelBaixo(temp4[0]);
            movePixelAtraz(temp4[0]);
            movePixelAtraz(temp4[0]);
            movimento = 1;
            break;
            }
    }
}

// Gira o Objeto L Atual
const giraL = () => {
    switch (movimento)
    {
        case 1:
            if(verificaColPixel(temp1[0],40,"baixo"))
                break;
            else if(verificaColPixel(temp1[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp2[0],20,"frente"))
                break;
            else
            {
            movePixelBaixo(temp1[0]);
            movePixelBaixo(temp1[0]);
            movePixelAtraz(temp1[0]);
            movePixelFrente(temp2[0]);
            movimento = 2;
            break;
            }
        case 2:
            if(verificaColPixel(temp3[0],20,"frente"))
                break;
            else if(verificaColPixel(temp3[0],40,"cima"))
                break;
            else if(verificaColPixel(temp1[0],20,"frente"))
                break;
            else if(verificaColPixel(temp1[0],40,"cima"))
                break;
            else
            {
            movePixelFrente(temp3[0]);
            movePixelCima(temp3[0]);
            movePixelCima(temp3[0]);
            movePixelFrente(temp1[0]);
            movePixelCima(temp1[0]);
            movePixelCima(temp1[0]);
            movimento = 3;
            break;
            }
        case 3:
            if(verificaColPixel(temp1[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp1[0],40,"baixo"))
                break;
            else if(verificaColPixel(temp3[0],40,"atraz"))
                break;
            else if(verificaColPixel(temp3[0],20,"baixo"))
                break;
            else if(verificaColPixel(temp4[0],20,"cima"))
                break;
            else if(verificaColPixel(temp4[0],20,"atraz"))
                break;
            else
            {
            movePixelAtraz(temp1[0]);
            movePixelBaixo(temp1[0]);
            movePixelBaixo(temp1[0]);
            movePixelAtraz(temp3[0]);
            movePixelAtraz(temp3[0]);
            movePixelBaixo(temp3[0]);
            movePixelCima(temp4[0]);
            movePixelAtraz(temp4[0]);
            movimento = 4;
            break;
            }
        case 4:
            if(verificaColPixel(temp1[0],40,"cima"))
                break;
            else if(verificaColPixel(temp1[0],20,"frente"))
                break;
            else if(verificaColPixel(temp2[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp3[0],20,"frente"))
                break;
            else if(verificaColPixel(temp3[0],20,"baixo"))
                break;
            else if(verificaColPixel(temp4[0],20,"frente"))
                break;
            else if(verificaColPixel(temp4[0],20,"baixo"))
                break;
            {
            movePixelCima(temp1[0]);
            movePixelCima(temp1[0]);
            movePixelFrente(temp1[0]);
            movePixelAtraz(temp2[0]);
            movePixelFrente(temp3[0]);
            movePixelBaixo(temp3[0]);
            movePixelFrente(temp4[0]);
            movePixelBaixo(temp4[0]);
            movimento = 1;
            break;
            }
    }

}

//Gira o Obejto S Atual
const giraS = () => {
    switch (movimento)
    {
        case 1:
            if(verificaColPixel(temp3[0],20,"frente"))
                break;
            else if(verificaColPixel(temp4[0],40,"cima"))
                break;
            else if(verificaColPixel(temp4[0],20,"frente"))
                break;
            else
            {
            movePixelFrente(temp3[0]);
            movePixelCima(temp4[0]);
            movePixelCima(temp4[0]);
            movePixelFrente(temp4[0]);
            movimento = 2;
            break;
            }
        case 2:
            if(verificaColPixel(temp3[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp4[0],40,"baixo"))
                break;
            else if(verificaColPixel(temp4[0],20,"atraz"))
                break;
            else
            {
            movePixelAtraz(temp3[0]);
            movePixelBaixo(temp4[0]);
            movePixelBaixo(temp4[0]);
            movePixelAtraz(temp4[0]);
            movimento = 1;
            }
    }
}

//Gira o Objeto Z Atual
const giraZ = () => {
    switch (movimento)
    {
        case 1:
            if(verificaColPixel(temp1[0],40,"frente"))
                break;
            else if(verificaColPixel(temp4[0],40,"cima"))
                break;
            else
            {
            movePixelFrente(temp1[0]);
            movePixelFrente(temp1[0]);
            movePixelCima(temp4[0]);
            movePixelCima(temp4[0]);
            movimento = 2;
            break;
            }
        case 2:
            if(verificaColPixel(temp1[0],40,"atraz"))
                break;
            else if(verificaColPixel(temp4[0],40,"baixo"))
                break;
            else
            {
            movePixelAtraz(temp1[0]);
            movePixelAtraz(temp1[0]);
            movePixelBaixo(temp4[0]);
            movePixelBaixo(temp4[0]);
            movimento = 1;
            }
    }
}

// Gira o Objeto T Atual
const giraT = () => {
    switch (movimento)
    {
        case 1:
            if(verificaColPixel(temp4[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp4[0],20,"baixo"))
                break;
            else
            {
            movePixelAtraz(temp4[0]);
            movePixelBaixo(temp4[0]);
            movimento = 2;
            break;
            }
        case 2:
            if(verificaColPixel(temp1[0],20,"frente"))
                break;
            else if(verificaColPixel(temp1[0],20,"baixo"))
                break;
            else
            {
            movePixelFrente(temp1[0]);
            movePixelBaixo(temp1[0]);
            movimento = 3;    
            break;
            }
        case 3:
            if(verificaColPixel(temp2[0],20,"cima"))
                break;
            else if(verificaColPixel(temp2[0],20,"frente"))
                break;
            else
            {
            movePixelCima(temp2[0]);
            movePixelFrente(temp2[0]);
            movimento = 4;
            break;
            }
        case 4:
            if(verificaColPixel(temp1[0],20,"cima"))
                break;
            else if(verificaColPixel(temp1[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp2[0],20,"baixo"))
                break;
            else if(verificaColPixel(temp2[0],20,"atraz"))
                break;
            else if(verificaColPixel(temp4[0],20,"cima"))
                break;
            else if(verificaColPixel(temp4[0],20,"frente"))
                break;
            else
            {
            movePixelCima(temp1[0]);
            movePixelAtraz(temp1[0]);
            movePixelBaixo(temp2[0]);
            movePixelAtraz(temp2[0]);
            movePixelCima(temp4[0]);
            movePixelFrente(temp4[0]);
            movimento = 1;            
            break;
            }
    }
}

// Girador do Objeto Atual
const giraObj = () => {
    switch (tipoObj)
    {
        case "I":
            giraI();
            break;
        case "S":
            giraS();
            break;
        case "Z":
            giraZ();
            break;
        case "T":
            giraT();
            break;
        case "L":
            giraL();
            break;
    }
}

//Variaveis para a Captura Posicao Atual o Objeto
let poy = [0, 0, 0, 0];
let pox = [0, 0, 0, 0];

// Captura a Posicao do Objeto Atual
const captaPosObj = () => {
    pox[1] = parseInt(document.getElementById(temp1[0]).style.left);
    pox[2] = parseInt(document.getElementById(temp2[0]).style.left);
    pox[3] = parseInt(document.getElementById(temp3[0]).style.left);
    pox[4] = parseInt(document.getElementById(temp4[0]).style.left);
    
    poy[1] = parseInt(document.getElementById(temp1[0]).style.top);
    poy[2] = parseInt(document.getElementById(temp2[0]).style.top);
    poy[3] = parseInt(document.getElementById(temp3[0]).style.top);
    poy[4] = parseInt(document.getElementById(temp4[0]).style.top);
    //window.alert(poy[1]+";"+poy[2]+";"+poy[3]+";"+poy[4]); 
}

//verifica Colisao do Objeto com pixeis Abaixo
function verificaColBaixo() {
    let Px = 0;
    let Py = 0;
    captaPosObj();

    for (let i = 1;i <= 4;i++) {
        if(poy[i] === 380) {
            return true
        }
    }

    for (let i = 1;i < (quantPixel-3);i++) {
        let col = document.getElementById(`p${i}`)
        Px = parseInt(col.style.left);
        Py = parseInt(col.style.top);
        for (let j = 1;j <= 4;j++) {
            if( (pox[j] === Px) && ((poy[j] + 20) === Py) ) {
                return true
            }
        }            
    }
}

//verifica Colisao do Objeto com pixeis Frente
const verificaColFrente = () => {
    let Px = 0;
    let Py = 0;
    captaPosObj();

    for (let i = 1;i <= 4;i++) {
        if(pox[i] == 180) {
            return true
        }
    }
    for (let i = 1;i < (quantPixel-3);i++) {
        let col = document.getElementById(`p${i}`)
        Px = parseInt(col.style.left);
        Py = parseInt(col.style.top);
        for (let j = 1;j <= 4;j++) {
            if( (poy[j] == Py) && ((pox[j] + 20) == Px) ) return true;
        }
    }
}

//verifica Colisao do Objeto com pixeis Frente
const verificaColAtraz = () => {
    let Px = 0;
    let Py = 0;
    captaPosObj();

    for (let i = 1;i <= 4;i++) {
        if(pox[i] == 0) {
            return true
        }
    }
    for (let i = 1;i < (quantPixel-3);i++) {
        Px = parseInt(document.getElementById(`p${i}`).style.left);
        Py = parseInt(document.getElementById(`p${i}`).style.top);
        for (let j = 1;j <= 4;j++) {
            if( (poy[j] == Py) && ((pox[j] - 20) == Px) ) {
                return true
            }
        }
    }
}

//Limita e detecta colisoes em eixo X
const limitaMoveFrente = () => {
    if(!verificaColFrente())
        moveFrenteObj();
}

const limitaMoveAtraz = () => {
    if(!verificaColAtraz())
        moveAtrazObj();
}

// DETECTOR DE LINHAS COMPLETAS //////////////////////////////////////////////////////////////////////

//desce linhas pixeis
const descePixeis = posit => {
    let Px = 0;
    let Py = 0;
    let score = document.getElementById("score")
    score.innerHTML = parseInt(score.innerHTML) + 100;
    // switch (parseInt(document.getElementById("score").innerHTML)) {
    //     case 500:
    //         document.getElementById("conteudo").style.backgroundColor = "#00FF00";
    //         break;
    //     case 1000:
    //         document.getElementById("conteudo").style.backgroundColor = "#0000FF";
    //         break;
    //     case 1500:
    //         document.getElementById("conteudo").style.backgroundColor = "#FFFF00";
    //         break;
    //     case 2000:
    //         document.getElementById("conteudo").style.backgroundColor = "#00FFFF";
    //         break;
    //     case 2500:
    //         document.getElementById("conteudo").style.backgroundColor = "#FF00FF";
    //         break;
    //     case 3000:
    //         document.getElementById("conteudo").style.backgroundColor = "#C0C0C0";
    //         break;
    //     case 3500:
    //         document.getElementById("conteudo").style.backgroundColor = "#FFFFFF";
    //         break;
    //     case 4000:
    //         alert("VOCE GANHOU!!! PARABENS!!!");
    //         window.location.reload();
    //         break;
    // }

    
    for (let i = 1;i < (quantPixel-3);i++) {
        let col = document.getElementById(`p${i}`)
        Px = parseInt(col.style.left);
        Py = parseInt(col.style.top);
        
        if(Py === posit) {
            col.style.top = "400px";
            col.style.display = "none";
        }

        if(Py < posit) {
            movePixelBaixo(`p${i}`)
        }  
    }
}

//Detecta Linha Completa
const buscaLinhas = () => {
    let totalX = 0;
    for (let i = 380;i > 0;i -= 20) {    
        totalX = 0;
        for (let j = 1;j < (quantPixel-3);j++) {
            Py = parseInt(document.getElementById(`p${j}`).style.top);
            if(Py === 0) {
                window.location.reload()
            }
            if(Py === i) {
                totalX++
            }  
        }
        if(totalX == 10) {
            descePixeis(i)
        }
    }
 
}

// Funcao Final
const Play = () => {
    buscaLinhas();
    if (!verificaColBaixo()) {
        desceObj();
    }
    else {
        if (downVar) {
            clearInterval(timer2)
        }
        downVar = false;
        movimento = 1;
        geraObjRandom();  
    } 
}

////CONTROLE DE OBJETO PELO TECLADO /////////////////////////////////////////////////////////////////////
let downVar = false;
const verifaTeclaPress = (e) => {
    let codTecla;
    if(window.event)
        codTecla = window.event.keyCode;
    else
        if(e)
            codTecla = e.which;
            
        //verifica tecla
        switch (codTecla)
        {
            case 39:             // --->
                limitaMoveFrente();        
                break;
            case 37:             // <---
                limitaMoveAtraz();
                break;
            case 38:             // UP
                giraObj();
                break;
            case 40:
                timer2 = setInterval("Play()",10);
                downVar = true;
                
        }
}
const verifaTeclaPress2 = e => {
    let codTecla;
    if(window.event)
        codTecla = window.event.keyCode;
    else
        if(e)
            codTecla = e.which;
            
        //verifica tecla
        switch (codTecla)
        {
            case 40:
                clearInterval(timer2);
                downVar = true;
                
        }
}
// CARREGA EVENTOS NO BOTOES /////////////////////////////////////////////////////////////////////////////////////
const redimen = () => {
    tamanhoWin = document.body.clientWidth;
    tamanhoDisplay = document.getElementById("conteudo").clientWidth;
    tamanhobtPlay = document.getElementById("btPlay").clientWidth;
    document.getElementById("conteudo").style.left = parseInt(tamanhoWin / 2) - parseInt(tamanhoDisplay / 2);
    document.getElementById("pontos").style.left = parseInt(tamanhoWin / 2) + parseInt(tamanhoDisplay / 2) + 10;
    document.getElementById("btPlay").style.left = parseInt(tamanhoWin / 2) - parseInt(tamanhobtPlay / 2);
}

const carrega = () => {
    const start = document.getElementById("btPlay")
    start.addEventListener("click", () => {
        geraObjRandom();
        timer1 = setInterval("Play()",1000);
        document.getElementById("btPlay").style.display="none";
        document.onkeydown = verifaTeclaPress;
        document.onkeyup = verifaTeclaPress2;
    })
   redimen();
}
carrega();
// window.onresize = redimen;
